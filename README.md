Réalisation d'un jeu du pendu en javascript et ASP.Net.

Règles du jeu : 
-   On peut ajouter/supprimer/modifier un mot dans le dictionnaire.
-   Les mots ajoutées ne doivent pas contenir d'accents.

-   Un mot est sélectionné aléatoirement dans ceux existant dans la base de données.
-   Le joueur a 10 chances pour trouver les lettres.
-   Si le joueur trouve une bonne lettre toute les lettres correspondant seront affichées.
-   Si le joueur se trompe il perd une chance.
-   Quand le joueur a finis la partie un message apparait selon le contexte (victoire ou défaite) et le joueur ne peut plus sélectionner de lettres.
-   Le joueur peut relancer une partie à tout moment avec un bouton.


La partie dictionnaire contient le code pour créer et récupérer les mots pour le jeu.

La page Home/index contient le code javascript du jeu.