﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace kata_pendu.Models
{
    public class kata_penduContext : DbContext
    {
        public kata_penduContext (DbContextOptions<kata_penduContext> options)
            : base(options)
        {
        }

        public DbSet<kata_pendu.Models.Dictionnary> Dictionnary { get; set; }
    }
}
