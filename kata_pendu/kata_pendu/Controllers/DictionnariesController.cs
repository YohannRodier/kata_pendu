﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using kata_pendu.Models;
using System.Text;
using System.Globalization;

namespace kata_pendu.Controllers
{
    public class DictionnariesController : Controller
    {
        private readonly kata_penduContext _context;

        public DictionnariesController(kata_penduContext context)
        {
            _context = context;
        }

        // GET: Dictionnaries
        public async Task<IActionResult> Index()
        {
            return View(await _context.Dictionnary.ToListAsync());
        }

        // GET: Dictionnaries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dictionnary = await _context.Dictionnary
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dictionnary == null)
            {
                return NotFound();
            }

            return View(dictionnary);
        }

        // GET: Dictionnaries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Dictionnaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Word")] Dictionnary dictionnary)
        {
            if (ModelState.IsValid)
            {
                dictionnary.Word = RemoveDiacritics(dictionnary.Word);
                _context.Add(dictionnary);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dictionnary);
        }

        // GET: Dictionnaries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dictionnary = await _context.Dictionnary.FindAsync(id);
            if (dictionnary == null)
            {
                return NotFound();
            }
            return View(dictionnary);
        }

        // POST: Dictionnaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Word")] Dictionnary dictionnary)
        {
            if (id != dictionnary.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dictionnary);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DictionnaryExists(dictionnary.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dictionnary);
        }

        // GET: Dictionnaries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dictionnary = await _context.Dictionnary
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dictionnary == null)
            {
                return NotFound();
            }

            return View(dictionnary);
        }

        // POST: Dictionnaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dictionnary = await _context.Dictionnary.FindAsync(id);
            _context.Dictionnary.Remove(dictionnary);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<JsonResult> GetRandomWord()
        {
            Random random = new Random();
            var dictionnary = await _context.Dictionnary.ToListAsync();
            int rand = random.Next(dictionnary.Count());
            var word = dictionnary[rand];
            return Json(word);
        }

        public string RemoveDiacritics(string str)
        {
            if (null == str) return null;
            var chars =
                from c in str.Normalize(NormalizationForm.FormD).ToCharArray()
                let uc = CharUnicodeInfo.GetUnicodeCategory(c)
                where uc != UnicodeCategory.NonSpacingMark
                select c;

            var cleanStr = new string(chars.ToArray()).Normalize(NormalizationForm.FormC);

            return cleanStr;
        }

        private bool DictionnaryExists(int id)
        {
            return _context.Dictionnary.Any(e => e.Id == id);
        }
    }
}
